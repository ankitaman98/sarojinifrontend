import React, { Component } from 'react';

import Navbar from './navbar/Navbar'
import {BrowserRouter} from 'react-router-dom';

import ImageBar from './ImageBar';

// import Donatics from './Donatics';


import Subscribe from './Subscribe';

import Footer from './footers/Footer';

function AllApp() {
  return (
    <BrowserRouter>
     <div className="apps">
      <Navbar/>
      {/* <ImageBar/>
      <Footer/> 
      <Subscribe/> */}
      {/* <Donatics/>
      <Subscribe/>
      <Footer/> */}
    </div>
    </BrowserRouter>
   
  );
}

export default AllApp;