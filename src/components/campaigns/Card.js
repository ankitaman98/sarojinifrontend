import React, { Component } from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';


import './campaignCss.css';



function Card(props){
    return(
        <div className="cardTop">
  <Container-fluid>
  <Row>
    <Col className="text-left card">
    <div className="transparenBox">
    <div className="imageBox">
    <Image src={props.imgsrc}/>
    <div class="unsuccess">Unsuccessful</div>
    </div>
    </div>
    <div>
        <h3>{props.title}</h3>
        <h5>{props.dateInfo}</h5>
        <p>{props.paraInfo}</p>
    </div>
    </Col>
  </Row>
</Container-fluid>
</div>
    );
}

export default Card;