import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

 import './ChangeLiveCss.css';



function ChangeLive(){
    return(
        <div className="changeLive">
        <Container>
        <Row>
          <Col className="text-center" >
          <h2>Change Lives With Your Gift</h2>
        <p>Giving a donation to Donatics can help us to reach more children transform their lives</p> 
        <p>for the better. Join your hand with us for a better life and beautiful future.</p>
          </Col>
        </Row>
        <Row className="m-0">
          <Col lg={3} ></Col>
          <Col sm={6} md={3} lg={3} md="auto" className="text-center but1">
            <button>DONATE NOW</button>
          </Col>
          <Col sm={6} md={3} lg={3} md="auto" className="text-center but2"> 
          <button>GET INVOLVED</button>
          </Col>
          <Col lg={3}></Col>
        </Row>
      </Container>
      </div>
    );
}


export default ChangeLive;
