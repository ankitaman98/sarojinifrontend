import React, { Component } from 'react';

import CampaignSrcData from './CampaignSrcData';
import Card from './Card';
import Container from 'react-bootstrap/esm/Container';
import Col from 'react-bootstrap/esm/Col';
import Row from 'react-bootstrap/esm/Row';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';


const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
    slidesToSlide: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1600 },
    items: 3,
    slidesToSlide: 3
  },
  tablet: {
    breakpoint: { max: 1600, min: 1000 },
    items: 2,
    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 1000, min: 0 },
    items: 1,
    slidesToSlide: 1
  }
};




function Cards(){
    return(
      <Carousel swipeable={false}
      draggable={true}
      showDots={true}
      arrows={false}
      responsive={responsive}>
  <div>
  <Card
                        imgsrc={CampaignSrcData[0].imgsrc}
                        title={CampaignSrcData[0].title}
                        dateInfo={CampaignSrcData[0].dateInfo}
                        paraInfo={CampaignSrcData[0].paraInfo}
      />
  </div>
  <div>
  <Card
                        imgsrc={CampaignSrcData[1].imgsrc}
                        title={CampaignSrcData[1].title}
                        dateInfo={CampaignSrcData[1].dateInfo}
                        paraInfo={CampaignSrcData[1].paraInfo}
      />
  </div>
  <div> <Card
                        imgsrc={CampaignSrcData[2].imgsrc}
                        title={CampaignSrcData[2].title}
                        dateInfo={CampaignSrcData[2].dateInfo}
                        paraInfo={CampaignSrcData[2].paraInfo}
      />
      </div>
  <div> <Card
                        imgsrc={CampaignSrcData[3].imgsrc}
                        title={CampaignSrcData[3].title}
                        dateInfo={CampaignSrcData[3].dateInfo}
                        paraInfo={CampaignSrcData[3].paraInfo}
      />
      </div>
      <div> <Card
                        imgsrc={CampaignSrcData[4].imgsrc}
                        title={CampaignSrcData[4].title}
                        dateInfo={CampaignSrcData[4].dateInfo}
                        paraInfo={CampaignSrcData[4].paraInfo}
      />
      </div>
      <div> <Card
                        imgsrc={CampaignSrcData[5].imgsrc}
                        title={CampaignSrcData[5].title}
                        dateInfo={CampaignSrcData[5].dateInfo}
                        paraInfo={CampaignSrcData[5].paraInfo}
      />
      </div>
</Carousel>
    );
}

export default Cards;
