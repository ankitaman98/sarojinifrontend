import React, { Component } from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';

import './RecentNewsCardCss.css';



function RecentNewsCard(props){
    return(
        <div >
  <Container>
  <Row>
    <Col className="text-left recentNewsCardBg pl-5">
      <div>
      <div className="uncategorise">
        <span className="uncategorisesp">
        <a href={props.uncategorisedLink}>UNCATEGORISED</a>
        </span>
        <span>
          <a href={props.dateLink}> - JULY 2,2019</a>
        </span>
      </div>
    <h3 style={{fontWeight:"bolder",fontSize:"30px"}}><a href={props.titleLink}>{props.title}</a></h3>
    <div className="byLine">By- 
    <span>
      <a href={props.byyLink}>CHUNG PHAM</a>
      </span>
    </div>
      </div>
    </Col>
  </Row>
</Container>
</div>
    );
}

export default RecentNewsCard;