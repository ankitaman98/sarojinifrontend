import React, { Component } from 'react';

import CampaignSrcData from './CampaignSrcData';
import RecentNewsCard from './RecentNewsCard';
import Container from 'react-bootstrap/esm/Container';
import Col from 'react-bootstrap/esm/Col';
import Row from 'react-bootstrap/esm/Row';
import Image from 'react-bootstrap/Image';


const RecentNewsSrcData=[
    {
        imgsrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        titleLink:"#",
        title:"A Day to Inspire 2018 with Ian Winson",
        uncategorisedLink:"#",
        dateLink:"#",
        byyLink:"#"

    },
    {
        imgsrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        titleLink:"#",
        title:"Book Launch 2016 with Tony Quinn",
        uncategorisedLink:"#",
        dateLink:"#",
        byyLink:"#"

    },
    {
        imgsrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        titleLink:"#",
        title:"A Day to Inspire 2017 with Rob Matthews",
        uncategorisedLink:"#",
        dateLink:"#",
        byyLink:"#"

    },
]
function RecentNewsCards(){
    return(
      <div>
      <Container-fluid>
          <Row className="m-0">
            <Col md={12} lg={6} xl={4}>
            <RecentNewsCard
                        imgsrc={RecentNewsSrcData[0].imgsrc}
                        titleLink={RecentNewsSrcData[0].titleLink}
                        title={RecentNewsSrcData[0].title}
                        uncategorisedLink={RecentNewsSrcData[0].uncategorisedLink}
                        dateLink={RecentNewsSrcData[0].dateLink}
                        byyLink={RecentNewsSrcData[0].byyLink}
      />
            </Col>
            <Col  md={12} lg={6} xl={4}>
            <RecentNewsCard
                        imgsrc={RecentNewsSrcData[1].imgsrc}
                        titleLink={RecentNewsSrcData[0].titleLink}
                        title={RecentNewsSrcData[1].title}
                        uncategorisedLink={RecentNewsSrcData[1].uncategorisedLink}
                        dateLink={RecentNewsSrcData[1].dateLink}
                        byyLink={RecentNewsSrcData[1].byyLink}
      />
            </Col>
            <Col lg={12} lg={6} xl={4}>
            <RecentNewsCard
                        imgsrc={RecentNewsSrcData[2].imgsrc}
                        titleLink={RecentNewsSrcData[0].titleLink}
                        title={RecentNewsSrcData[2].title}
                        uncategorisedLink={RecentNewsSrcData[2].uncategorisedLink}
                        dateLink={RecentNewsSrcData[2].dateLink}
                        byyLink={RecentNewsSrcData[2].byyLink}
      />
            </Col>
          </Row>
        </Container-fluid>
        </div>
    );
}
export default RecentNewsCards;
