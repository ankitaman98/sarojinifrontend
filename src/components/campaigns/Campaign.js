import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


import './campaignCss.css';

import Cards from './Cards';

import Impacts from './Impacts'; 

import ChangeLive from './ChangeLive';
import LogoCompany from './LogoCompany';
import RecentNewsCards from './RecentNewsCards';
import Insta from './Insta';


function Campaign(){
    return(
    <>
      <div className="bg">
        <Container>
          <Row>
            <Col><h4 style={{fontSize:"40px",fontWeight:"bold"}}>Our Campaign</h4>
            </Col>
          </Row>
        </Container>
        <Cards/>
        </div>
      <div className="impactTop">
      <Impacts/>
      </div>
      <Insta/>
      <div style={{marginTop:"150px",marginBottom:"250px"}}>
        <h1 style={{textAlign:"center", marginTop:"100px",marginBottom:"50px",fontSize:"60px",fontWeight:"bold"}}>Recent News</h1>
        <RecentNewsCards/>
      </div>
      
      <div className="changeLiveBg">
      <ChangeLive/>
      </div>
        <LogoCompany/>
    </>
  
    );
}

export default Campaign;