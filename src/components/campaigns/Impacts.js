import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CountUp from 'react-countup';

import './campaignCss.css';



function Impacts(){
    return(
        <div className="impact">
        <Container>
        <Row>
          <Col className="text-center" >
          <h2>Our Impact In Numbers</h2>
        <p >If you have a particular cause you are passionate about,</p> 
        <p style={{paddingBottom:"60px"}}>Donatics is the perfect place to start!</p>
          </Col>
        </Row>
        <Row>
          <Col lg={12} xl={4} className="text-center csc">
            <p style={{fontSize:"80px",fontWeight:"bold"}}>
            <CountUp start={0} end={49}/>+
            </p>
            COUNTRIES
            </Col>
          <Col lg={12} xl={4} className="text-center csc">
          <p style={{fontSize:"80px",fontWeight:"bold"}}>
          <CountUp start={0} end={15}/>+</p>
          SUCCESS CAMPAIGN
            </Col>
          <Col lg={12} xl={4} className="text-center csc">
          <p style={{fontSize:"80px",fontWeight:"bold"}}>
          <CountUp start={0} end={117}/>+</p>
            CAMPAIGN RUNNING
            </Col> 
        </Row>
      </Container>
      </div>
    );
}

export default Impacts;