import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

 import './LogoCompanyCss.css';



function LogoCompany(){
    return(
         <div className="logoCampany">
        <Container-fluid>
        <Row className="m-0">
          <Col sm={6} lg={4} xl={2} className="text-center logo mt-5"> 
          <a href="#">BOOKSTORE LOGO</a>
          </Col >
          <Col sm={6} lg={4} xl={2} className="text-center logo mt-5"> 
          <a href="#">RETRO LOGO</a>
          </Col>
          <Col sm={6} lg={4} xl={2} className="text-center logo mt-5"> 
          <a href="#">VINTAGE LOGO</a>
          </Col>
          <Col sm={6} lg={4} xl={2} className="text-center logo mt-5"> 
          <a href="#">RONALD VISUAL STUDIO  LOGO</a>
          </Col>
          <Col sm={6} lg={4} xl={2} className="text-center logo mt-5"> 
          <a href="#">RETRO  LOGO</a>
          </Col>
          <Col sm={6} lg={4} xl={2} className="text-center logo mt-5"> 
          <a href="#">VINTAGE  LOGO</a>
          </Col>
        </Row>
      </Container-fluid>
       </div>
    );
}


export default LogoCompany;