import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CountUp from 'react-countup';

import './aboutUsCssFile/AboutUsCss.css';

import Image from 'react-bootstrap/Image';

function AboutUs(){
    return(
        <div className="aboutUsBg">
            <Container-fluid>
                <Row className="m-0 aboutUsColDiv">
                    <Col md={6} className="aboutUsCol ">
                    <div className="aboutUs">
                    <span>
                      <a href="#"><span>Worldlife </span></a>
                    </span>
                    > 
                    <span> About Us</span>
                    </div>
                    </Col>
                    <Col md={6} md="auto">
                    <div>
                       <h1 style={{fontWeight:"bold"}}>About us</h1>
                    </div>
                    </Col>
                </Row>
            </Container-fluid>
        </div>
    );
}

function GlobalLeader(){
    return(
        <div className="globalLeaderDiv">
        <Container-fluid>
            <Row className="m-0 globalLeaderRow">
                <Col xl={6} className="text-center">
                    <div>
                    <div className="globalLeaderBg" >
                    <img style={{height:"80vh"}} src="http://demo2.themelexus.com/worldlife/wp-content/uploads/2019/07/h2-bg01.jpg"
                     className="img-fluid" alt="Responsive image"/>
                      </div>
                    <div className="globalLeaderBgIn"> 
                    <img style={{height:"80vh"}} src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg"
                     className="img-fluid" alt="Responsive image"/>
                    </div>
                    </div>
                </Col>
                <Col xl={6}>
                    <div className="globalLeaderClm">
                    <div>
                        <h2>
                            WE ARE A GLOBAL LEADER WITHIN A WORLDWIDE MOVEMENT DEDICATED TO ENDING POVERTY</h2>
                    </div>
                    <div>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                         sed do eiusmod tempor incididunt ut labore et dolore
                         magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        <br></br>
                        <br></br>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                         fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                          culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde 
                          omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam 
                          rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto 
                          beatae vitae dicta sunt explicabo.
                        </p>
                    </div>
                    </div>
                </Col>
            </Row>
        </Container-fluid>
        </div>
    );
}


function Mission(props){
    return(
        <div>
            <Container>
                <Row>
                    <Col>
                    <div>
                        <h3 style={{fontWeight:"bolder",marginTop:"100px",marginBottom:"20px",fontSize:"35px"}}>
                            {props.title}</h3>
                    </div>
                    <div>
                        <p style={{fontSize:"23px",lineHeight:"35px"}}>{props.paraInfo}</p>
                    </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

const missionArrayDataSrc=[
    {
        title:"OUR MISSION",
        paraInfo:"We work around the globe to save lives, defeat poverty and achieve social justice."
    },
    {
        title:"OUR VISION",
        paraInfo:"We seek a world of hope, tolerance and social justice, where poverty has been overcome and all people live with dignity and security."
    },
    {
        title:"OUR VALUES",
        paraInfo:"We believe in urgent action, innovation, and the necessity of transformation—within the world and our own organization."
    }
]

function MissionArray(){
    return(
        <Container-fluid>
            <Row className="m-0">
                <Col lg={4}>
                <Mission
                title={missionArrayDataSrc[0].title}
                paraInfo={missionArrayDataSrc[0].paraInfo}
                />
                </Col>
                <Col lg={4}>
                <Mission
                title={missionArrayDataSrc[1].title}
                paraInfo={missionArrayDataSrc[1].paraInfo}
                />
                </Col>
                <Col lg={4}>
                <Mission
                title={missionArrayDataSrc[2].title}
                paraInfo={missionArrayDataSrc[2].paraInfo}
                />
                </Col>
            </Row>
        </Container-fluid>

    );
}



function WhatWeDo(){
    return(
        <div className="whatWeDoBg">
            <div style={{textAlign:"center",padding:"100px 0 100px 0"}}>
                <h2 style={{fontWeight:"bold",fontSize:"40px"}}>WHAT WE DO</h2>
            </div>
            <Container-fluid>
                <Row className="m-0"> 
                    <Col sm={12} lg={6} xl={3} className="text-center">
                    <div className="whatWeDoCol">
                    <img style={{height:"100px"}} src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg"
                     className="img-fluid" alt="Responsive image"/>
                        <div className="whatWeDoColCnt">
                        <p style={{fontSize:"80px",fontWeight:"bold"}}>
                        <CountUp start={0} end={17}/>
                        </p>
                        </div>
                    </div>
                    <div style={{fontWeight:"bold",fontSize:"20px"}}>
                    Year of 
                    <br></br>
                    Experience
                    </div>
                    <p style={{marginBottom:"40px",fontSize:"20px"}}>
                    Veniam, quis nostrud exercitation ullamco
                     laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                    </Col>
                    <Col sm={12} lg={6} xl={3} className="text-center">
                    <div className="whatWeDoCol">
                    <img style={{height:"100px"}} src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg"
                     className="img-fluid" alt="Responsive image"/>
                        <div className="whatWeDoColCnt">
                        <p style={{fontSize:"80px",fontWeight:"bold"}}>
                        <CountUp start={0} end={24}/>
                        </p>
                        </div>
                    </div>
                    <div style={{fontWeight:"bold",fontSize:"20px"}}>
                    Country 
                    </div>
                    <p style={{marginBottom:"40px",fontSize:"20px"}}>Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                    
                    </Col>
                    <Col sm={12} lg={6} xl={3} className="text-center">
                    <div className="whatWeDoCol">
                    <img style={{height:"100px"}} src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg"
                     className="img-fluid" alt="Responsive image"/>
                        <div className="whatWeDoColCnt">
                        <p style={{fontSize:"80px",fontWeight:"bold"}}>
                        <CountUp start={0} end={395}/>
                        </p>
                        </div>
                    </div>
                    <div style={{fontWeight:"bold",fontSize:"20px"}}>
                    Campaigns
                    </div>
                  <p style={{marginBottom:"40px",fontSize:"20px"}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>
                    </Col>
                    <Col sm={12} lg={6} xl={3} className="text-center">
                    <div className="whatWeDoCol">
                    <img style={{height:"100px"}} src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg"
                     className="img-fluid" alt="Responsive image"/>
                        <div className="whatWeDoColCnt">
                        <p style={{fontSize:"80px",fontWeight:"bold"}}>
                        <CountUp start={0} end={54}/>+
                        </p>
                        </div>
                    </div>
                    <div style={{fontWeight:"bold",fontSize:"20px"}}>
                        Million people
                        <br></br>
                    Helped
                    </div>
                        <p style={{marginBottom:"40px",fontSize:"20px"}}>Veniam, quis nostrud exercitation ullamco laboris
                             nisi ut aliquip ex ea commodo consequat.</p>
                    </Col>
                </Row>
            </Container-fluid>
        </div>
    );
}


function OurTeam(props){
    return(
        <div className="ourTeam">
            <Container>
                <Row>
                    <Col className="text-center">
                    <div className="ourTeamImage">
                    <img src={props.imgSrc} className="img-fluid" alt="Responsive image"/>
                    <div className="ourTeamTitle">
                           <div className="name"> {props.name}</div>
                            <div className="post">{props.post}</div>
                    </div>
                    
                        <div className="OTLogoList">
                            <ul>
                                <li><a href="#">fb</a></li>
                                <li><a href="#">In</a></li>
                                <li><a href="#">Yt</a></li>
                                <li><a href="#">Ld</a></li>
                            </ul>
                        </div>
                    
                    </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

const OurTeamArray=[
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"Angela Shelton",
        post:"CEO/Founder"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"William P.",
        post:"Manager"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"John Doe",
        post:"Leader"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"Mary Jordan",
        post:"Founder"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"Emma Becker",
        post:"Volunteer"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"Mike Carter",
        post:"Volunteer"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"Mia James",
        post:"CEO"
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
        name:"Lisa Cruz",
        post:"Volunteer"
    },
]

function MeetOurTeam(){
    return(
    <div className="meetOurTeam">
<div className="ourTeamHead"><h2>MEET OUR TEAM</h2></div>
        <Container-fluid>
            <Row className="m-0">
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[0].imgSrc}
                name={OurTeamArray[0].name}
                post={OurTeamArray[0].post}
                />
                </Col>
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[1].imgSrc}
                name={OurTeamArray[1].name}
                post={OurTeamArray[1].post}
                />
                </Col>
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[2].imgSrc}
                name={OurTeamArray[2].name}
                post={OurTeamArray[2].post}
                />
                </Col>
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[3].imgSrc}
                name={OurTeamArray[3].name}
                post={OurTeamArray[3].post}
                />
                </Col>
            </Row>
            <Row className="m-0">
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[4].imgSrc}
                name={OurTeamArray[4].name}
                post={OurTeamArray[4].post}
                />
                </Col>
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[5].imgSrc}
                name={OurTeamArray[5].name}
                post={OurTeamArray[5].post}
                />
                </Col>
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[6].imgSrc}
                name={OurTeamArray[6].name}
                post={OurTeamArray[6].post}
                />
                </Col>
                <Col md={12} lg={6} xl={3}>
                <OurTeam
                imgSrc={OurTeamArray[7].imgSrc}
                name={OurTeamArray[7].name}
                post={OurTeamArray[7].post}
                />
                </Col>
            </Row>
        </Container-fluid>
        <div className="BOVTop">
        <Container>
            <Row>
                <Col></Col>
                <Col md="auto" className="text-center">
                <div>
                    <a href="#">
                        <div className="BOV">
                        <span > BECOME </span>
                        <span> OUR </span>
                        <span> VOLUNTEER </span>
                        </div>
                    </a>
                </div>
                </Col>
                <Col></Col>
            </Row>
        </Container>
        </div>
    </div>
    );
}


function Sponser(){
    return(
        <div className="ourSponser">
            <div><h1>OUR SPONSORS</h1></div>
        <Container-fluid>
        <Row className="m-0">
          <Col sm={6} lg={4} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">BOOKSTORE LOGO</a>
          </Col >
          <Col sm={6} lg={4} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">RETRO LOGO</a>
          </Col>
          <Col sm={6} lg={4} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">VINTAGE LOGO</a>
          </Col>
          <Col sm={6} md={6} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">RONALD VISUAL STUDIO  LOGO</a>
          </Col>
        </Row>
        <Row className="m-0">
          <Col sm={6} md={6} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">BOOKSTORE LOGO</a>
          </Col >
          <Col sm={6} md={6} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">RETRO LOGO</a>
          </Col>
          <Col sm={6} md={6} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">VINTAGE LOGO</a>
          </Col>
          <Col sm={6} md={6} lg={3} className="text-center ourSponserCol mt-5"> 
          <a href="#">RONALD VISUAL STUDIO  LOGO</a>
          </Col>
        </Row>
      </Container-fluid>
       </div>
    );
}

export default AboutUs;
export {GlobalLeader, MissionArray,WhatWeDo,MeetOurTeam,Sponser};
