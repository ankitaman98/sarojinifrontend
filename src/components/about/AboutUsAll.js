import React, { Component } from 'react';

import AboutUs,{GlobalLeader, MissionArray, WhatWeDo, MeetOurTeam, Sponser} from './AboutUs';



function AboutUsAll(){
    return(
        <>
        <AboutUs/>
        <GlobalLeader/>
        <MissionArray/>
        <WhatWeDo/>
        <MeetOurTeam/>
        <Sponser/>
        </>
    );
}

export default AboutUsAll;
