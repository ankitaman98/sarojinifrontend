import React, { Component } from 'react';

import Contact from './Contact';
import About from './About';
import Campaign from './Campaign';
import Blog from './Blog';
import Events from './Events';
import Home from './Home';
import Page from './Page';
import {Route, Switch } from 'react-router-dom';
import Donation from './Donation';
import Menu from './Menu';


import './Navbarcss.css';


function Navbar(){
    return(
      
        <div className='top'>
            <span className="worldlife">🤔 Worldlife</span>
            <Menu/>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route exact path='/About' component={About}/>
                <Route exact path='/Blog' component={Blog}/>
                <Route exact path='/Events' component={Events}/>
                <Route exact path='/Campaign' component={Campaign}/>
                <Route exact path='/Contact' component={Contact}/>
                <Route exact path='/Page' component={Page}/>
                <Route exact path='/Donation' component={Donation}/>
            </Switch>
            
        </div>
    );
}

export default Navbar;
