import React, { Component } from 'react';

import ContactUs, {AddressForm} from './ContactUs';


function ContactUsAll(){
    return(
        <>
        <ContactUs/>
        <AddressForm/>
        </>
    );
}

export default ContactUsAll;