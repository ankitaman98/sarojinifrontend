import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CountUp from 'react-countup';

import './ContactUsCss.css';

import Image from 'react-bootstrap/Image';

function ContactUs(){
    return(
        <div className="contactUsBg">
            <Container-fluid>
                <Row className="m-0 contactUsColDiv">
                    <Col md={6} className="contactUsCol ">
                    <div className="contactUs">
                    <span>
                      <a href="#"><span>Worldlife </span></a>
                    </span>
                    > 
                    <span> Contact Us</span>
                    </div>
                    </Col>
                    <Col md={6} md="auto">
                    <div>
                       <h1 style={{fontWeight:"bold"}}>Contact us</h1>
                    </div>
                    </Col>
                </Row>
            </Container-fluid>
        </div>
    );
}


function AddressForm(){
    return(
        <div className="addressDivTop">
        <Container-fluid>
            <Row className="m-0">
            
                <Col lg={6} className="col-xl-7">
                <div className="addressCol">
                    <div className="headingDiv">
                        <h2 >
                        We’d love to hear from you! Give us call,
                         send us a message? or find us on social media.
                        </h2>
                    </div>
                    <div className="addressDiv">
                        <p>ADDRESS</p>
                    </div>
                    <div className="addressDivP">
                        <p>12356 Glassford Street Glasgow G1 1UL <span>New York, USA</span></p>
                    </div>

                    <div className="addressDiv">
                        <p>EMAIL US</p>
                    </div>
                    <div className="addressDivP">
                    <p><a href="#">contact @example.com</a></p>
                    </div>

                    <div className="addressDiv">
                    <p>phone number</p>
                    </div>
                    <div className="addressDivP">
                    <p>+844 123 456 78</p> 
                    </div>

                    <div className="addressDiv">
                    <p>social</p>  
                    </div>
                    <div className="addressDivP">
                    <ul className="">
                            <li className="">
                                <a href="#" target="_blank"> <span class="">Facebook</span></a>
                            </li>
                            <li className="">
                                 <a href="#" target="_blank"><span class="">Twitter</span></a>
                            </li>
                            <li className="">
                                 <a href="#" target="_blank"><span class="">Instagram</span> </a>
                            </li>
                    </ul>
                    </div>
                    </div>
                </Col>
               
                <Col lg={6} className="col-xl-5">
                <div className="formCol2">
                <div className="Col2Heading">
                    <h2>SIGN UP FORM</h2>
                </div>
                <div className="Col2HeadP">
                <p>The following info is required</p>
                </div>
                <form>
                <div className="formGroup">
                        <label for="exampleInputName">YOUR NAME<span style={{color:"#ff0000",fontSize:"20px"}}>*</span></label>
                        <input type="text" class="form-control" id="exampleInputPassword1"></input>
                    </div>
                    <div className="formGroup">

                        <label for="exampleInputEmail1">YOUR EMAIL<span style={{color:"#ff0000",fontSize:"20px"}}>*</span></label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" ></input>
                        
                    </div>
                   
                    <div className="formGroup">
                        <label for="exampleInputEmail1">PHONE NUMBER<span style={{color:"#ff0000",fontSize:"20px"}}>*</span></label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"></input>
                        
                    </div>
                       <div className="formGroup">     
                    <label> YOUR MESSAGE<br></br>
                        <span class="wpcf7-form-control-wrap your-message">
                        <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="4"></textarea>
                        </span> </label>
                     </div> 
                    <div className="summit">
                    <input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit"></input>
                    </div>
                    </form>
                    </div>
                </Col>
            </Row>
        </Container-fluid>
        <section style={{marginTop:"30px"}}>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d115133.01016889236!2d85.07300151213991!3d25.608020764037622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f29937c52d4f05%3A0x831a0e05f607b270!2sPatna%2C%20Bihar!5e0!3m2!1sen!2sin!4v1597306766423!5m2!1sen!2sin" 
            style={{width:"100%", height:"80vh", frameborder:"0", border:"0"}} ></iframe>
            </section>
        </div>
    );
}


export default ContactUs;
export {AddressForm};