import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import BlogCard from './BlogCard';

import './BlogCss.css';

// import Image from 'react-bootstrap/Image';

function Blog(){
    return(
        <div className="blogBg">
            <Container-fluid>
                <Row className="m-0 blogColDiv">
                    <Col md={6} className="blogCol ">
                    <div className="blog">
                    <span>
                      <a href="#"><span>Worldlife </span></a>
                    </span>
                    > 
                    <span> Blog </span>
                    </div>
                    </Col>
                    <Col md={6} md="auto">
                    <div>
                       <h1 style={{fontWeight:"bold"}}>Blog</h1>
                    </div>
                    </Col>
                </Row>
            </Container-fluid>
        </div>
    );
}



const BlogContentArray=[
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"A Day to Inspire 2018 with Ian Winson",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"Book Launch 2016 with Tony Quinn",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"A Day to Inspire 2017 with Rob Matthews",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"A Day to Inspire 2016 with Mark Inglis",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"A Day to Inspire 2018 with Tony Christiansen",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"A Day to Inspire 2018 with Ian Winson",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"Race4Life Track Day Hampton Downs",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    },
    {
        imgSrc:"https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg",
       title:"Tuesday Tips: Being Realistic With Your Goals",
        paraInfo:"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    }
    
]


function BlogContent(){
    return(
        <div>
        <div className="blogContentAll">
            <Container-fluid>
                <Row className="m-0">
                    <Col sm={12} className=" col-sm-12 col-xl-8">
                        <div className="">
                            <div>
                             <BlogCard
                             imgSrc={BlogContentArray[0].imgSrc}
                             title={BlogContentArray[0].title}
                             paraInfo={BlogContentArray[0].paraInfo}
                             />
                            </div>
                            <div >
                            <BlogCard
                             imgSrc={BlogContentArray[1].imgSrc}
                             title={BlogContentArray[1].title}
                             paraInfo={BlogContentArray[1].paraInfo}
                             />
                            </div>
                            <div >
                            <BlogCard
                             imgSrc={BlogContentArray[2].imgSrc}
                             title={BlogContentArray[2].title}
                             paraInfo={BlogContentArray[2].paraInfo}
                             />
                            </div>
                            <div >
                            <BlogCard
                             imgSrc={BlogContentArray[3].imgSrc}
                             title={BlogContentArray[3].title}
                             paraInfo={BlogContentArray[3].paraInfo}
                             />
                            </div>
                            <div>
                            <BlogCard
                             imgSrc={BlogContentArray[4].imgSrc}
                             title={BlogContentArray[4].title}
                             paraInfo={BlogContentArray[4].paraInfo}
                             />
                            </div>
                            <div >
                            <BlogCard
                             imgSrc={BlogContentArray[5].imgSrc}
                             title={BlogContentArray[5].title}
                             paraInfo={BlogContentArray[5].paraInfo}
                             />
                            </div>
                            <div>
                            <BlogCard
                             imgSrc={BlogContentArray[6].imgSrc}
                             title={BlogContentArray[6].title}
                             paraInfo={BlogContentArray[6].paraInfo}
                             />
                            </div>
                            <div>
                            <BlogCard style={{border:"none"}}
                             imgSrc={BlogContentArray[7].imgSrc}
                             title={BlogContentArray[7].title}
                             paraInfo={BlogContentArray[7].paraInfo}
                             />
                            </div>
                        </div>
                    </Col>
                     <Col sm={12} className=" col-sm-12 col-xl-4">
                    <div className="colmn2 sticky-top" >
                    <section className="searchBox">
                    <form>
                <div class="form-group emailtags">
               <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="search..."></input>
               <button type="submit" className="btn">search</button>
                </div>
            </form>
                    </section>
                    <section className="recentPost">
                    <h2 className="RPtitle">Recent Posts</h2>
                    <ul>
                        <li class="item-recent-post">
                        <span class="thumbnail-post"><img width="100" height="100" src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg" class="" alt=""  sizes="(max-width: 100px) 100vw, 100px"></img></span>
                         <span class="title-post">
                        <a href="#">A Day to Inspire 2018 with Ian Winson</a>
                        </span>
                        </li>
                        <li class="item-recent-post">
                        <span class="thumbnail-post"><img width="100" height="100" src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg" class="" alt="" sizes="(max-width: 100px) 100vw, 100px"></img></span>
                        <span class="title-post">
                        <a href="#">Book Launch 2016 with Tony Quinn</a>
                        </span>
                    </li>
                    <li class="item-recent-post">
                        <span class="thumbnail-post"><img width="100" height="100" src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg" class="" alt="" sizes="(max-width: 100px) 100vw, 100px"></img></span>
                        <span class="title-post">
                        <a href="#">A Day to Inspire 2017 with Rob Matthews</a>
                        </span>
                    </li>
                        <li class="item-recent-post">
                         <span class="thumbnail-post"><img width="100" height="100" src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg" class="" alt="" sizes="(max-width: 100px) 100vw, 100px"></img></span>
                         <span class="title-post">
                        <a href="#">A Day to Inspire 2016 with Mark Inglis</a>
                        </span>
                    </li>
               <li style={{border:"none"}} class="item-recent-post">
                        <span class="thumbnail-post"><img width="100" height="100" src="https://i.ibb.co/V9Ck6qs/photo-1593642532842-98d0fd5ebc1a-ixlib-rb-1-2.jpg" class="" alt="" sizes="(max-width: 100px) 100vw, 100px"></img></span>
                        <span class="title-post">
                        <a href="#">A Day to Inspire 2018 with Tony Christiansen</a>
                        </span>
                </li>
            </ul>
                    </section>
                    <section className="recentComment">

                    <h2 class="widget-title">Recent Comments</h2>
                    <ul id="recentcomments">
                        <li style={{border:"none"}} class="recentcomments"><span class="comment-author-link"><a href="#" rel="external nofollow" class="url">A WordPress Commenter</a></span>
                     on <a href="#">Tuesday Tips: Being Realistic With Your Goals</a>
                     </li>
                     </ul>
                    </section>
                    <section className="arhive">
                    <h2 class="widget-title">Archives</h2>
                    <ul>
				        <li style={{border:"none"}} ><a href="#">July 2019</a></li>
		            </ul>
                    </section>
                    <section className="categories">
                    <h2 class="widget-title">Categories</h2>
                    <ul>
				            <li class="cat-item cat-item-7"><a href="#">Children</a></li>
                            <li class="cat-item cat-item-8"><a href="#">Education</a></li>
                            <li class="cat-item cat-item-9"><a href="#">Food &amp; health</a></li>
                            <li class="cat-item cat-item-10"><a href="#">Homeless</a></li>
                            <li class="cat-item cat-item-11"><a href="#">Medical</a></li>
                            <li style={{border:"none"}} class="cat-item cat-item-1"><a href="#">Uncategorized</a></li>
		            </ul> 
                    </section>
                    <section className="meta">
                    <h2 class="widget-title">Meta</h2>
                    <ul>
						<li><a href="#">Log in</a></li>
			            <li><a href="#">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			            <li><a href="#">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			            <li style={{border:"none"}} ><a href="#" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li></ul>
                    </section>
                    </div>
                    </Col> 
                </Row>
            </Container-fluid>
        </div>
        </div>
    );
}

export default Blog;
export {BlogContent};