import React, { Component } from 'react';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// import Image from './react-bootstrap';


import './BlogCss.css';

function BlogCard(props){
    return(
        <div className="colmn">
        <Container-fluid>
                <Row>
                    <Col className="colmn1">
                        <div className="colmn11">
                            <div className="colmn111Image">
                            <img style={{width:"100%"}} src={props.imgSrc} class="img-rounded" alt="Cinque Terre"></img>
                            </div> 
                            <div className="colmn11Header">
                                <div className="UCDate">
                                    <span className="uncategorised">
                                    <a href="#">UNCATEGORISED</a>
                                    </span> - 
                                    <span className="dateInfo">
                                    <a href="#">July 18, 2019</a>  
                                    </span>
                                </div>
                                <h2>
                                <a href="#">{props.title}</a>
                                </h2>
                            </div> 
                            <div className="colmn11ParaInfo">
                                <p>{props.paraInfo}</p>
                                <div className="readMore">
                            <Container-fluid>
                                <Row>
                                    <Col md="auto" className="text-center">
                                    <div>
                                        <a href="#">
                                            <div className="BOV">
                                            <span > READ </span>
                                            <span> MORE </span>
                                            </div>
                                        </a>
                                    </div>
                                    </Col>
                                   
                                </Row>
                            </Container-fluid>
                            </div>
                            </div>  
                            </div>
                    </Col>
                    </Row>
                    </Container-fluid>
                    </div>
    );
}

export default BlogCard;

