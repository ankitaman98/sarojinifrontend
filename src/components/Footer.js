import React, { Component } from 'react';
import "../App.css";

import Footer1 from './footers/Footer1';
import Footer2 from './footers/Footer2';

import Footer3 from './footers/Footer3';

import Footer4 from './footers/Footer4';

function Footer(){
    return(
        <div className="backgroundfooter">
      <div>
      <Footer1/>
      </div>
      <div>
      <Footer2/>
      </div>
      <div>
      <Footer3/>
      </div>
         <div>
        <Footer4/>
        </div> 
        </div>
    );
}

export default Footer;