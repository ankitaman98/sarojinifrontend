import React, { Component } from 'react';


import Donatics from './Donatics';

import ImageBoxParent from './ImageBoxParent';

function DonaticsParent(){
    return(
        <div>
        <div className="DonaticsParent">
            <Donatics/>
            <ImageBoxParent/>
        </div>
        </div>
    );
}