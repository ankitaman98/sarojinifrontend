import React, { Component } from 'react';
import Container from "react-bootstrap/Container";

function Footer2(){
    return(
        <div>
        <Container>
            <div className="row footer2">
            <div className="col-xl-6 footer21">
            <div className="row">
                <div className="col xs={12} textcenter">
                <h5>DON’T MISS LATEST UPDATES</h5>
                <p>Get in-context advice from our experts about your most pressing issues or areas of interest</p>
                <form>
                <div class="form-group emailtags">
               <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email address"></input>
               <button type="submit" className="btn">Submit</button>
       </div>
            </form>
                </div>
                {/* <div className="col text-center">
                </div>
                <div className="col-md-auto text-center">
                </div> */}
        </div>
            </div>              
                                        <div className="col-xl-6 item-center">
                                    <div className="container-fliud">
                                        <div className="row footer22">
                                <div className="col xs={12}  text-left">
                                            <h6>INFO</h6>
                                            <ul>
                                        <li> <a href="#" >Our Courses</a></li>
                                        <li> <a href="#" >Volunteer</a></li>
                                        <li> <a href="#" >Events</a></li>
                                        <li> <a href="#" >Careers</a></li>
                                        <li> <a href="#" >Our Mission</a></li>
                                    </ul>
                                            </div>
                                <div className="col xs={12}  text-left">
                                            <h6>OUR CAMPAINS</h6>
                                            <ul>
                                        <li> <a href="#" >Healthy Food</a></li>
                                        <li> <a href="#" >Medical</a></li>
                                        <li> <a href="#" >Pure Water</a></li>
                                        <li> <a href="#" >Education</a></li>
                                    </ul>
                                            </div>
                                <div className="col xs={4} text-left">
                                            <h6>USEFUL LINKS</h6>
                                            <ul>
                                        <li> <a href="#" >Get Involved</a></li>
                                        <li> <a href="#" >News</a></li>
                                        <li> <a href="#" >FAQs</a></li>
                                        <li> <a href="#" >Contact Us</a></li>
                                    </ul>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
            </div>
            </Container>
        </div>
    );
}

export default Footer2;