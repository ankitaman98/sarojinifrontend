import React, { Component } from 'react';
import Container from "react-bootstrap/Container";

import './FooterCss.css';


function Footer3(){
    return(
        <>
        <Container>
            <div className="row footer3">
            <div className="col text-center"></div>
                <div className="col-md-auto text-center">
                  <div className="container-fluid">  
                   <div className="row">
                            <div className="col-md-auto text-center">
                                <a href="#">FACEBOOK</a>
                            </div>
                            <div className="col-md-auto text-center">
                            <a href="#">TWITTER</a>
                            </div>
                            <div className="col-md-auto text-center">
                            <a href="#">YOUTUBE</a>
                            </div>
                            <div className="col-md-auto text-center">
                            <a href="#">GOOGLE</a>
                            </div>
                            </div>
                    </div>
                </div>
                <div className="col text-center"></div>
            </div>
            
       </Container>
        </>
    );
}

export default Footer3;