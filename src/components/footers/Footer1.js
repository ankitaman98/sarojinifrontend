import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';

import './FooterCss.css';
function Footer1(){
    return(
        <>
        {/* <div className="container-fluid"> */}
            <Container>
            <div className="row footer1">
                <div className="col-md-auto text-center">
                <h1>WorldLife</h1>
                </div>
                <div className="col text-center">
                <p>44 Shirley Ave. West Chicago, IL 60185, USA | 1800 – 123 456 78 | contact @example.com</p>
                </div>
                <div className="col-md-auto text-center">
                    <form>
                    <button type="submit" className="btn"><a href="#">DONATE NOW</a></button>
                    </form>
                </div>
            </div>
            </Container>
        {/* </div> */}
        </>
    );
}

export default Footer1;