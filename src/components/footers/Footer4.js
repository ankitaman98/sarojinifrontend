import React, { Component } from 'react';
import Container from "react-bootstrap/Container";


import './FooterCss.css';

function Footer4(){
    return(
       <>
        <Container>
            <div className="row footer4">
                <div className="col-md-auto text-center">
                <p>Copyright © 2019 Worldlife. All Rights Reserved.</p>
                </div>
                <div className="col text-center"></div>
                <div className="col-md-auto text-center">
                <p>Carefully crafted by Themelexus.</p>
                </div>
            </div>
        </Container>
        </>
    );
}

export default Footer4;